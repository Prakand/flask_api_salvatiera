from flask import Flask, jsonify, request
from flask_restful import Api, Resource

app = Flask(__name__)
api = Api(app)


def check_posted_data(postedData, functionName):
    if functionName == "add" or functionName == "sub" or functionName == "mult":
        if "x" not in postedData or "y" not in postedData:
            return 301
        else:
            return 200
    elif functionName == "division":
        if "x" not in postedData or "y" not in postedData:
            return 301
        elif int(postedData["y"]) == 0:
            return 302
        else:
            return 200


class Add(Resource):
    def post(self):
        postedData = request.get_json()
        status_code = check_posted_data(postedData, "add")
        if status_code != 200:
            retJson = {
                "message": "Some error occurred",
                "status": status_code
            }
            return jsonify(retJson)

        x = postedData["x"]
        y = postedData["y"]
        x = int(x)
        y = int(y)
        ret = x + y
        retMap = {
            'sum': ret,
            "status": 200
        }
        return retMap


class Subtract(Resource):
    def post(self):
        postedData = request.get_json()
        status_code = check_posted_data(postedData, "sub")
        if status_code != 200:
            retJson = {
                "message": "invalid input",
                "status": status_code
            }
            return jsonify(retJson)

        x = postedData["x"]
        y = postedData["y"]
        x = int(x)
        y = int(y)
        ret = x - y
        retMap = {
            "Sub": ret,
            "status": 200
        }
        return retMap


class Multiply(Resource):
    def post(self):
        postedData = request.get_json()
        status_code = check_posted_data(postedData, "mult")
        if status_code != 200:
            retJson = {
                "message": "Invalid Input",
                "status": status_code
            }
            return jsonify(retJson)

        x = postedData['x']
        y = postedData['y']
        x = int(x)
        y = int(y)
        ret = x * y
        retMap = {
            "mult": ret,
            "status": 200
        }
        return retMap


class Divide(Resource):
    def post(self):
        postedData = request.get_json()
        status_code = check_posted_data(postedData, "mult")
        if status_code != 200:
            retJson = {
                "message": "Invalid Input",
                "status": status_code
            }
            return jsonify(retJson)

        x = postedData['x']
        y = postedData['y']
        x = int(x)
        y = int(y)
        ret = x / y
        retMap = {
            "mult": ret,
            "status": 200
        }
        return retMap


api.add_resource(Add, '/add')
api.add_resource(Subtract, "/sub")
api.add_resource(Multiply, "/mult")
api.add_resource(Divide, "/division")


@app.route('/')
def hello_word():
    return {"Message": "Flask api setup"}


if __name__ == "__main__":
    app.run(host='0.0.0.0')
